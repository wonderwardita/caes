<?php 
    session_start();
?>
<!DOCTYPE html>
<head>
<title>Log in</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
</head>
    <body>
        <center><br><br>
            <form class="" method="post" action="admin_login_proc.php">
                <div class="container">
                    <h2 class="tittle">LOGIN</h2><br>
                    <?php
                    if (isset($_SESSION['alert_msg'])) {
                        if ($_SESSION['alert_msg']==2) {
                            echo "<h4 align=center>**USERNAME OR PASSWORD IS INCORRECT!</h4>";
                            session_unset();
                        } 
                    }
                    ?>
                    <input type="text" placeholder="Username" name="username" class="username" required><br><br>
                    <input type="password" name="password" placeholder="Password" class="password" required><br><br><br>
                    <a href="#"><button type="submit" class="login">LOG IN</button></a><br><br>
                    <br>
                </div>
            </form>
        </center>
    </body>
</html>