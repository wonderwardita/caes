<?php 
    session_start();
    include "perfect_function.php";

    $table_name = "users";

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = _hash_string($_POST['password']);
    $account_type = $_POST['account_type'];
    $account_status = $_POST['account_status'];

    $user_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "email" => $email,
        "username" => $username,
        "password" => $password,
    );
    
    echo insert($user_data, $table_name);
    $_SESSION['alert_msg'] = 1;
    header("Location: signup.php")
?>