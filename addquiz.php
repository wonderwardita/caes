<!DOCTYPE html>
<html>
<head>
    <title>Computer Aided Examination System</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
</head>
    <body class="back">
        <center>
            <form method="post" action="addquiz_proc.php">
                <div class="container">
                    <table border="0">
                        <h2 class="form1">ADD QUESTION</h2><hr>
                        <tr>
                            <td align="center"><textarea  cols="50"  name="question" placeholder="Type question here..."></textarea></td>
                        </tr>
                        <tr>
                            <td><input type="text"  name="optionA" placeholder="Option A" required/></td>
                        </tr>
                        <tr>
                            <td><input type="text"  name="optionB" placeholder="Option B" required/></td>
                        </tr>
                        <tr>
                            <td><input type="text"  name="optionC" placeholder="Option C" required/></td>
                        </tr>
                        <tr>
                        <td><button> SUBMIT</button></td>
                        </tr>
                        <tr>
                    </table> 
                </div>
            </form> 
        </center>  
    </body>  
</html>  